package com.epam.rd.java.basic.task8.contacts;

public class Constants {
    public static final String CLASS__DOCUMENT_BUILDER_FACTORY_INTERNAL =
            "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl";
    public static final String CLASS__SAX_PARSER_FACTORY_INTERNAL =
            "com.sun.org.apache.xerces.internal.jaxp.SAXParserFactoryImpl";
    public static final String FEATURE__TURN_VALIDATION_ON =
            "http://xml.org/sax/features/validation";
    public static final String FEATURE__TURN_SCHEMA_VALIDATION_ON =
            "http://apache.org/xml/features/validation/schema";
    public static final String MY_NS__URI = "http://www.nure.ua";
    public static final String MY_NS__PREFIX = "n";

    public static final String SCHEMA_LOCATION__URI = "http://www.nure.ua input.xsd ";
    public static final String JAXB__PACKAGE = "com.epam.rd.java.basic.task8.entity";
    public static final String XSD_FILE_NAME = "input.xsd";
    public static final String INVALID_XML_FILE = "invalidXML.xml";
    public static final String XML_FILE_NAME = "input.xml";

    public static final String XSI_SCHEMA_LOCATION__ATTR_FQN = "xsi:schemaLocation";

//    public static final String SCHEMA_LOCATION__URI =
//            "http://knure.kharkov.ua/jt/st3example input.xsd";
}
