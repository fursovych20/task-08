package com.epam.rd.java.basic.task8.util;

import com.epam.rd.java.basic.task8.entity.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class Sorted {
    /**
     * Sort flower by name
     */
    public static final Comparator<Flower> sortFlowersByName = new Comparator<Flower>() {
        @Override
        public int compare(Flower o1, Flower o2) {
            return o1.getName().compareTo(o2.getName());
        }
    };
    /**
     * Sort flower by visual parameters measure (AveLenFlower)
     */
    public static final Comparator<Flower> sortByAveLenFlower = new Comparator<Flower>() {
        @Override
        public int compare(Flower o1, Flower o2) {
            return o1.getVisualParameters().getAveLenFlower().getValue().compareTo(o2.getVisualParameters().getAveLenFlower().getValue());
        }
    };
    /**
     *Sort by temperature
     */
    public static final Comparator<Flower> sortByTemperature = new Comparator<Flower>() {
        @Override
        public int compare(Flower o1, Flower o2) {
            return o1.getGrowingTips().getTempreture().getValue().compareTo(o2.getGrowingTips().getTempreture().getValue());
        }
    };

    /**
     * Sort by lighting
     */
    public static final Comparator<Flower> sortLightingByLightRequiring = new Comparator<Flower>() {
        @Override
        public int compare(Flower o1, Flower o2) {
            return o1.getGrowingTips().getLighting().getLightRequiring().compareTo(o2.getGrowingTips().getLighting().getLightRequiring());
        }
    };

    // Sorting Flowers with support create comparators

    public static final void sortFlowersByName(Flowers flowers){
        Collections.sort(flowers.getFlower(),sortFlowersByName);
    }

    public static final void sortByTemperature(Flowers flowers){
        Collections.sort(flowers.getFlower(),sortByTemperature);
    }

    public static final void sortLightingByLightRequiring(Flowers flowers){
        Collections.sort(flowers.getFlower(),sortLightingByLightRequiring);
    }

    public static final void sortByAveLenFlower(Flowers flowers){
        Collections.sort(flowers.getFlower(),sortByAveLenFlower);
    }




}
