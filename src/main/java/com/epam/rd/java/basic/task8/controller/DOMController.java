package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.contacts.Constants;
import com.epam.rd.java.basic.task8.contacts.XMLEntity;
import com.epam.rd.java.basic.task8.entity.*;
import com.epam.rd.java.basic.task8.util.Transformer;
import com.epam.rd.java.basic.task8.util.XSDValidator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.math.BigInteger;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private final String xmlFileName;
    private final String xsdFileName;

    private Flowers flowers;  // -- this is container

    public DOMController(String xmlFileName, String xsdFileName) {
        this.xmlFileName = xmlFileName;
        this.xsdFileName = xsdFileName;
    }

    public Flowers getFlowers() {
        return flowers;
    }


    public void parse(boolean validate) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(
                Constants.CLASS__DOCUMENT_BUILDER_FACTORY_INTERNAL,
                this.getClass().getClassLoader());

        dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");

        dbf.setNamespaceAware(true);
        if (validate) {
            XSDValidator xsdValidator = new XSDValidator(xsdFileName);
            xsdValidator.validate(xmlFileName);
        }

        DocumentBuilder db = dbf.newDocumentBuilder();
        db.setErrorHandler(new DefaultHandler() {
            @Override
            public void error(SAXParseException e) throws SAXException {
                throw e;
            }
        });

        Document document = db.parse(xmlFileName);
        Element root = document.getDocumentElement();

        flowers = new Flowers(); // ---> container flowers

        NodeList flowersNodes = root.getElementsByTagName(XMLEntity.FLOWER.value());

        for (int i = 0; i < flowersNodes.getLength(); i++) {
            Flower flower = getFlower(flowersNodes.item(i));
            flowers.getFlower().add(flower);
        }
    }

    private Flower getFlower(Node node) {
        Flower flower = new Flower();
        String flowerName = getTextContent(node, XMLEntity.NAME);
        flower.setName(flowerName);
        String flowerSoil = getTextContent(node, XMLEntity.SOIL);
        flower.setSoil(flowerSoil);
        String flowerOrigin = getTextContent(node, XMLEntity.ORIGIN);
        flower.setOrigin(flowerOrigin);
        VisualParameters flowerVisualParameters = getVisualParameters(node);
        flower.setVisualParameters(flowerVisualParameters);
        GrowingTips flowerGrowingTips = getGrowingTips(node);
        flower.setGrowingTips(flowerGrowingTips);
        String flowerMultiplying = getTextContent(node, XMLEntity.MULTIPLYING);
        flower.setMultiplying(flowerMultiplying);
        return flower;
    }

    private GrowingTips getGrowingTips(Node gtNode) {
        GrowingTips flowerGrowingTips = new GrowingTips();
        if (gtNode.getNodeType() == Element.ELEMENT_NODE) {
            Element gtElement = (Element) gtNode;
            NodeList growingTipsNodes = gtElement.getElementsByTagName(XMLEntity.GROWING_TIPS.value());
            for (int j = 0; j < growingTipsNodes.getLength(); j++) {

                Tempreture tempreture = new Tempreture();
                tempreture.setMeasure(getAttribute(growingTipsNodes.item(j), XMLEntity.TEMPRETURE));
                tempreture.setValue(new BigInteger(getTextContent(growingTipsNodes.item(j), XMLEntity.TEMPRETURE)));
                flowerGrowingTips.setTempreture(tempreture);

                Lighting lighting = new Lighting();
                lighting.setLightRequiring(getAttribute(growingTipsNodes.item(j), XMLEntity.LIGHTING));
                flowerGrowingTips.setLighting(lighting);

                Watering watering = new Watering();
                watering.setMeasure(getAttribute(growingTipsNodes.item(j), XMLEntity.WATERING));
                watering.setValue(new BigInteger(getTextContent(growingTipsNodes.item(j), XMLEntity.WATERING)));
                flowerGrowingTips.setWatering(watering);
            }
        }
        return flowerGrowingTips;
    }

    private VisualParameters getVisualParameters(Node visualParametersNode) {

        VisualParameters visualParameters = new VisualParameters();

        if (visualParametersNode.getNodeType() == Element.ELEMENT_NODE) {

            Element vslPrElement = (Element) visualParametersNode;
            NodeList visualParametersNodes = vslPrElement.getElementsByTagName(XMLEntity.VISUAL_PARAMETERS.value());

            for (int j = 0; j < visualParametersNodes.getLength(); j++) {
                String stemColour = getTextContent(visualParametersNodes.item(j), XMLEntity.STEM_COLOUR);
                visualParameters.setStemColour(stemColour);

                String leafColour = getTextContent(visualParametersNodes.item(j), XMLEntity.LEAF_COLOUR);
                visualParameters.setLeafColour(leafColour);

                AveLenFlower aveLenFlower = new AveLenFlower();
                aveLenFlower.setMeasure(getAttribute(visualParametersNodes.item(j), XMLEntity.AVE_LEN_FLOWER));
                aveLenFlower.setValue(new BigInteger(getTextContent(visualParametersNodes.item(j), XMLEntity.AVE_LEN_FLOWER)));
                visualParameters.setAveLenFlower(aveLenFlower);
            }
        }
        return visualParameters;
    }

    private String getAttribute(Node node, XMLEntity xmlEntity) {

        Element aveLenFlowerElement = (Element) node;

        return aveLenFlowerElement.getElementsByTagName(xmlEntity.value()).item(0).getAttributes().item(0).getTextContent();
    }

    private String getTextContent(Node node, XMLEntity xmlEntity) {

        String elementTextContent = "";

        if (node.getNodeType() == Element.ELEMENT_NODE) {
            Element flowerElement = (Element) node;
            elementTextContent = flowerElement.getElementsByTagName(xmlEntity.value())
                    .item(0).getTextContent();
        }

        return elementTextContent;
    }

    public static void saveXML(Flowers flowers, String xmlFileName) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(Constants.CLASS__DOCUMENT_BUILDER_FACTORY_INTERNAL,
                Class.class.getClassLoader());

        dbf.setAttribute(javax.xml.XMLConstants.ACCESS_EXTERNAL_DTD, "");
        dbf.setAttribute(javax.xml.XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");

        dbf.setNamespaceAware(true);

        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.newDocument();

        Element fElement = document.createElementNS(
                Constants.MY_NS__URI, Constants.MY_NS__PREFIX + ":" + XMLEntity.FLOWERS.value());

        // set schema location
        fElement.setAttributeNS(
                XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI,
                Constants.XSI_SCHEMA_LOCATION__ATTR_FQN, Constants.SCHEMA_LOCATION__URI);

        document.appendChild(fElement);

        for (Flower flower : flowers.getFlower()) {
            addElementToXMLFile(flowers, document, fElement, flower);
        }
        Transformer.saveToXML(document, xmlFileName);
    }

    private static void addElementToXMLFile(Flowers flowers, Document document, Element fElement, Flower flower) {
        System.out.println(flowers.getFlower().size());
        Element flElement = document.createElement(Constants.MY_NS__PREFIX+":"+XMLEntity.FLOWER.value());

        fElement.appendChild(flElement);

        Element nameElement =
                document.createElement(Constants.MY_NS__PREFIX+":"+XMLEntity.NAME.value());
        nameElement.setTextContent(flower.getName());

        flElement.appendChild(nameElement);

        Element soilElement =
                document.createElement(Constants.MY_NS__PREFIX+":"+XMLEntity.SOIL.value());
        soilElement.setTextContent(flower.getSoil());

        flElement.appendChild(soilElement);

        Element originElement =
                document.createElement(Constants.MY_NS__PREFIX+":"+XMLEntity.ORIGIN.value());
        originElement.setTextContent(flower.getOrigin());
        flElement.appendChild(originElement);

        Element vp =
                document.createElement(Constants.MY_NS__PREFIX+":"+XMLEntity.VISUAL_PARAMETERS.value());
        Element stemColourElement =
                document.createElement(Constants.MY_NS__PREFIX+":"+XMLEntity.STEM_COLOUR.value());
        stemColourElement.setTextContent(flower.getVisualParameters().getStemColour());
        Element leafColourElement =
                document.createElement(Constants.MY_NS__PREFIX+":"+XMLEntity.LEAF_COLOUR.value());
        leafColourElement.setTextContent(flower.getVisualParameters().getLeafColour());
        Element aveLenFlowerElement =
                document.createElement(Constants.MY_NS__PREFIX+":"+XMLEntity.AVE_LEN_FLOWER.value());
        aveLenFlowerElement.setTextContent(flower.getVisualParameters().getAveLenFlower().getValue().toString());
        aveLenFlowerElement.setAttribute(XMLEntity.MEASURE.value(),
                flower.getVisualParameters().getAveLenFlower().getMeasure());

        flElement.appendChild(vp);

        vp.appendChild(stemColourElement);
        vp.appendChild(leafColourElement);
        vp.appendChild(aveLenFlowerElement);

        Element growingTipsElement =
                document.createElement(Constants.MY_NS__PREFIX+":"+XMLEntity.GROWING_TIPS.value());
        Element tempretureElement =
                document.createElement(Constants.MY_NS__PREFIX+":"+XMLEntity.TEMPRETURE.value());
        tempretureElement.setTextContent(flower.getGrowingTips().getTempreture().getValue().toString());
        tempretureElement.setAttribute(XMLEntity.MEASURE.value(), flower.getGrowingTips().getTempreture().getMeasure());
        Element lightingElement =
                document.createElement(Constants.MY_NS__PREFIX+":"+XMLEntity.LIGHTING.value());
        lightingElement.setAttribute(XMLEntity.LIGHT_REQUIRING.value(), flower.getGrowingTips().getLighting().getLightRequiring());
        Element wateringElement =
                document.createElement(Constants.MY_NS__PREFIX+":"+XMLEntity.WATERING.value());
        wateringElement.setTextContent(flower.getGrowingTips().getWatering().getValue().toString());
        wateringElement.setAttribute(XMLEntity.MEASURE.value(), flower.getGrowingTips().getWatering().getMeasure());

        flElement.appendChild(growingTipsElement);

        growingTipsElement.appendChild(tempretureElement);
        growingTipsElement.appendChild(lightingElement);
        growingTipsElement.appendChild(wateringElement);

        Element multiplyingElement =
                document.createElement(Constants.MY_NS__PREFIX+":"+XMLEntity.MULTIPLYING.value());
        multiplyingElement.setTextContent(flower.getMultiplying());

        flElement.appendChild(multiplyingElement);
    }

    public static void main(String[] args) throws ParserConfigurationException, IOException, TransformerException {
        DOMController domContr = new DOMController(Constants.XML_FILE_NAME, Constants.XSD_FILE_NAME);
        try {
            domContr.parse(true); // <-- parse with validation (failed)
            System.out.println(domContr.getFlowers());
        } catch (SAXException ex) {
            ex.printStackTrace();
            System.err.println("====================================");
            System.err.println("XML not valid");
            System.err.println("Test object --> " + domContr.getFlowers());
            System.err.println("====================================");
        }
        Flowers flowers1 = domContr.getFlowers();
        DOMController.saveXML(flowers1, "output" + ".dom.xml");
        // try to parse NOT valid XML document with validation off (success)
    }
}
