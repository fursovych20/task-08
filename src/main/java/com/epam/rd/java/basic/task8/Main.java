package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.contacts.Constants;
import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.util.Sorted;
import com.epam.rd.java.basic.task8.util.Transformer;

public class Main {

	public static void usage() {
		System.out
				.println("Usage: java -jar st3example.jar xmlFileName xsdFileName [xslFileName]");
	}
	
	public static void main(String[] args) throws Exception {
		if (args.length < 2 || args.length>3) {
			usage();
			return;
		}
		
		String xmlFileName = args[0];
		String xsdFileName = args[1];
		String xslFileName = null;
		if (args.length == 3)
			xslFileName = args[2];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName, xsdFileName);
		// PLACE YOUR CODE HERE
		domController.parse(true);
		Flowers flowers = domController.getFlowers();

		// sort (case 1)
		Sorted.sortFlowersByName(flowers);
		// PLACE YOUR CODE HERE
		// save
		String outputXmlFile = "output" + ".dom.xml";
		DOMController.saveXML(flowers, outputXmlFile);
		if (xslFileName != null)
			Transformer.saveToHTML(outputXmlFile, xslFileName, outputXmlFile
					+ ".html");
		// PLACE YOUR CODE HERE

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		saxController.parse(true);
		flowers = saxController.getFlowers();
		
		// sort  (case 2)
		Sorted.sortByTemperature(flowers);
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.sax.xml";
		DOMController.saveXML(flowers, outputXmlFile);

		if (xslFileName != null)
			Transformer.saveToHTML(outputXmlFile, xslFileName, outputXmlFile
					+ ".html");
		// PLACE YOUR CODE HERE
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		staxController.parse();
		flowers = staxController.getFlowers();
		
		// sort  (case 3)
		Sorted.sortByAveLenFlower(flowers);
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.stax.xml";
		DOMController.saveXML(flowers,outputXmlFile);
		// PLACE YOUR CODE HERE
		if (xslFileName != null)
			Transformer.saveToHTML(outputXmlFile, xslFileName, outputXmlFile
					+ ".html");
	}

}
