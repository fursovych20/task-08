package com.epam.rd.java.basic.task8.entity;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "visualParameters")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "stemColour",
        "leafColour",
        "aveLenFlower"
})
public class VisualParameters {

    @XmlElement(name = "stemColour", required = true)
    private String stemColour;
    @XmlElement(name = "leafColour", required = true)
    private String leafColour;
    @XmlElement(name = "aveLenFlower")
    private AveLenFlower aveLenFlower;

//    private VisualParameters

    public VisualParameters() {
    }

    public VisualParameters(String stemColour, String leafColour, AveLenFlower aveLenFlower) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlower = aveLenFlower;
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String value) {
        this.stemColour = value;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String value) {
        this.leafColour = value;
    }

    public AveLenFlower getAveLenFlower() {
        return aveLenFlower;
    }
    public void setAveLenFlower(AveLenFlower value) {
        this.aveLenFlower = value;
    }
    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                '}';
    }
}
