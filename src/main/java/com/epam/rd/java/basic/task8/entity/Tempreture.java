package com.epam.rd.java.basic.task8.entity;

import javax.xml.bind.annotation.*;
import java.math.BigInteger;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "value"
})
public class Tempreture {

    @XmlValue
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger value;
    @XmlAttribute(name = "measure", required = true)
    protected String measure;

    public Tempreture() {
    }

    public Tempreture(BigInteger value, String measure) {
        this.value = value;
        this.measure = measure;
    }

    public BigInteger getValue() {
        return value;
    }

    public void setValue(BigInteger value) {
        this.value = value;
    }

    public String getMeasure() {
        if (measure == null) {
            return "celcius";
        } else {
            return measure;
        }
    }
    public boolean isCorrect() {
        return measure != null;
    }

    public void setMeasure(String value) {
        this.measure = value;
    }

    @Override
    public String toString() {
        return "Tempreture{" +
                "value=" + value +
                ", measure='" + measure + '\'' +
                '}';
    }
}