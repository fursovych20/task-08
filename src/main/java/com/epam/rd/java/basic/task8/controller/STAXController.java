package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.contacts.Constants;
import com.epam.rd.java.basic.task8.contacts.XMLEntity;
import com.epam.rd.java.basic.task8.entity.*;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.math.BigInteger;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private final String xmlFileName;

    private String currentElement;

    private Flowers flowers;
    private Flower flower;

    private VisualParameters vp;
    private AveLenFlower aveLenFlower;

    private GrowingTips growingTips;
    private Tempreture tempreture;
    private Lighting lighting;
    private Watering watering;

    public Flowers getFlowers() {
        return flowers;
    }

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    /**
     * Parses XML document with StAX API. There is no validation during the
     * parsing.
     */
    public void parse() {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);
        XMLEventReader reader = null;

        try {
            reader = factory.createXMLEventReader(
                    new StreamSource(xmlFileName));
            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();

                //skip any empty content
                if (event.isCharacters() && event.asCharacters().isWhiteSpace()) {
                    continue;
                }

                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();
                    currentElement = startElement.getName().getLocalPart();

                    if (currentElement == XMLEntity.FLOWERS.value()) {
                        flowers = new Flowers();
                        continue;
                    }
                    if (currentElement == XMLEntity.FLOWER.value()) {
                        flower = new Flower();
                        continue;
                    }
                    if (currentElement == XMLEntity.VISUAL_PARAMETERS.value()) {
                        vp = new VisualParameters();
                        continue;
                    }
                    if (currentElement == XMLEntity.AVE_LEN_FLOWER.value()) {
                        aveLenFlower = new AveLenFlower();
                        Attribute attribute = startElement.getAttributeByName(
                                new QName(XMLEntity.MEASURE.value()));
//						System.out.println(attribute.getValue());
                        if (attribute != null) {
                            aveLenFlower.setMeasure(attribute.getValue());
                        }
                    }
                    if (currentElement == XMLEntity.GROWING_TIPS.value()) {
                        growingTips = new GrowingTips();
                    }
                    if (currentElement == XMLEntity.TEMPRETURE.value()) {
                        tempreture = new Tempreture();
                        Attribute attribute = startElement.getAttributeByName(
                                new QName(XMLEntity.MEASURE.value()));
                        if (attribute != null) {
                            tempreture.setMeasure(attribute.getValue());
//                            System.err.println(attribute.getValue());
                        }
                    }
                    if (currentElement == XMLEntity.LIGHTING.value()) {
                        lighting = new Lighting();
                        Attribute attribute = startElement.getAttributeByName(
                                new QName(XMLEntity.LIGHT_REQUIRING.value()));
//                        System.out.println(attribute.getValue());
                        if (attribute != null) {
                            lighting.setLightRequiring(attribute.getValue());
                            growingTips.setLighting(lighting);
                        }
                    }
                    if (currentElement == XMLEntity.WATERING.value()) {
                        watering = new Watering();
                        Attribute attribute = startElement.getAttributeByName(
                                new QName(XMLEntity.MEASURE.value()));
                        if (attribute != null) {
                            watering.setMeasure(attribute.getValue());
                        }
                    }
                }
                if (event.isCharacters()) {
                    Characters characters = event.asCharacters();

                    if (currentElement == XMLEntity.NAME.value()) {
                        flower.setName(characters.getData());
                        continue;
                    }
                    if (currentElement == XMLEntity.SOIL.value()) {
                        flower.setSoil(characters.getData());
                        continue;
                    }
                    if (currentElement == XMLEntity.ORIGIN.value()) {
                        flower.setOrigin(characters.getData());
                        continue;
                    }
                    if (currentElement == XMLEntity.STEM_COLOUR.value()) {
                        vp.setStemColour(characters.getData());
                        continue;
                    }
                    if (currentElement == XMLEntity.LEAF_COLOUR.value()) {
                        vp.setLeafColour(characters.getData());
                        continue;
                    }
                    if (currentElement == XMLEntity.AVE_LEN_FLOWER.value()) {
                        aveLenFlower.setValue(new BigInteger(characters.getData()));
                        vp.setAveLenFlower(aveLenFlower);
                        continue;
                    }
                    if (currentElement == XMLEntity.TEMPRETURE.value()) {
                        tempreture.setValue(new BigInteger(characters.getData()));
                        growingTips.setTempreture(tempreture);
                        continue;
                    }
                    if (currentElement == XMLEntity.WATERING.value()) {
                        watering.setValue(new BigInteger(characters.getData()));
                        growingTips.setWatering(watering);
                        continue;
                    }
                    if (currentElement == XMLEntity.MULTIPLYING.value()) {
                        flower.setMultiplying(characters.getData());
                        continue;
                    }


                }
                if (event.isEndElement()) {
//                    System.out.println(event.asEndElement().getName());
                    EndElement endElement = event.asEndElement();
                    String localName = endElement.getName().getLocalPart();

                    if (localName == XMLEntity.FLOWER.value()) {
                        flowers.getFlower().add(flower);
                        continue;
                    }
                    if (localName == XMLEntity.VISUAL_PARAMETERS.value()) {
                        flower.setVisualParameters(vp);
                        continue;
                    }
                    if (localName == XMLEntity.GROWING_TIPS.value()) {
                        flower.setGrowingTips(growingTips);
                    }
                }

            }

        } catch (XMLStreamException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (XMLStreamException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws ParserConfigurationException,
            SAXException, IOException, XMLStreamException {

        // try to parse (valid) XML file (success)
        STAXController staxContr = new STAXController(Constants.XML_FILE_NAME);
        staxContr.parse(); // <-- do parse (success)

        Flowers staxContrFlowers = staxContr.getFlowers(); // <-- obtain container

        // we have Test object at this point:
        System.out.println("====================================");
        System.out.println("Here is the test: \n" + staxContrFlowers);
        System.out.println("====================================");
    }
    // PLACE YOUR CODE HERE

}