package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.contacts.Constants;
import com.epam.rd.java.basic.task8.contacts.XMLEntity;
import com.epam.rd.java.basic.task8.entity.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.math.BigInteger;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private final String xmlFileName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void parse(boolean validate) throws SAXException, ParserConfigurationException, IOException {
		SAXParserFactory parserFactory = SAXParserFactory.newInstance(
				Constants.CLASS__SAX_PARSER_FACTORY_INTERNAL,
				this.getClass().getClassLoader()
		);

		parserFactory.setNamespaceAware(true);

		if (validate){
			parserFactory.setFeature(Constants.FEATURE__TURN_VALIDATION_ON, true);
			parserFactory.setFeature(Constants.FEATURE__TURN_SCHEMA_VALIDATION_ON,true);
		}

		SAXParser parser = parserFactory.newSAXParser();
		parser.parse(xmlFileName,this);
	}

	@Override
	public void error(org.xml.sax.SAXParseException e) throws SAXException {
		throw e; // <-- if XML document not valid just throw exception
	}


	private String currentElement;

	private Flowers flowers;
	private Flower flower;

	private VisualParameters vp;
	private AveLenFlower aveLenFlower;

	private GrowingTips growingTips;
	private Tempreture tempreture;
	private Watering watering;

	public Flowers getFlowers() {
		return flowers;
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		currentElement = localName;

		if (currentElement == XMLEntity.FLOWERS.value()){
			flowers = new Flowers();
			return;
		}
		if (currentElement == XMLEntity.FLOWER.value()){
			flower = new Flower();
			return;
		}
		if (currentElement == XMLEntity.VISUAL_PARAMETERS.value()){
			vp = new VisualParameters();
			return;
		}
		if (currentElement == XMLEntity.AVE_LEN_FLOWER.value()){
			aveLenFlower = new AveLenFlower();
			if (attributes.getLength()>0){
				aveLenFlower.setMeasure(attributes.getValue(0));
				System.out.println(attributes.getValue(0));
			}
		}
		if (currentElement == XMLEntity.GROWING_TIPS.value()){
			growingTips = new GrowingTips();
		}
		if (currentElement == XMLEntity.TEMPRETURE.value()){
			tempreture = new Tempreture();
			if (attributes.getLength()>0){
				tempreture.setMeasure(attributes.getValue(0));
			}
		}
		if (currentElement == XMLEntity.LIGHTING.value()){
			Lighting lighting = new Lighting();
			if (attributes.getLength()>0){
				lighting.setLightRequiring(attributes.getValue(0));
				growingTips.setLighting(lighting);
			}
		}
		if (currentElement == XMLEntity.WATERING.value()){
			watering = new Watering();
			if (attributes.getLength()>0){
				watering.setMeasure(attributes.getValue(0));
			}
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		String elementText = new String(ch,start,length).trim();

		if (elementText.isEmpty()){
			return;
		}
		if (currentElement == XMLEntity.NAME.value()){
			flower.setName(elementText);
			return;
		}
		if (currentElement == XMLEntity.SOIL.value()){
			flower.setSoil(elementText);
			return;
		}
		if (currentElement == XMLEntity.ORIGIN.value()){
			flower.setOrigin(elementText);
			return;
		}
		if (currentElement == XMLEntity.STEM_COLOUR.value()){
			vp.setStemColour(elementText);
			return;
		}
		if (currentElement == XMLEntity.LEAF_COLOUR.value()){
			vp.setLeafColour(elementText);
			return;
		}
		if (currentElement == XMLEntity.AVE_LEN_FLOWER.value()){
			aveLenFlower.setValue(new BigInteger(elementText));
			vp.setAveLenFlower(aveLenFlower);
			return;
		}
		if (currentElement == XMLEntity.TEMPRETURE.value()){
			tempreture.setValue(new BigInteger(elementText));
			growingTips.setTempreture(tempreture);
			return;
		}
		if (currentElement == XMLEntity.WATERING.value()){
			watering.setValue(new BigInteger(elementText));
			growingTips.setWatering(watering);
			return;
		}
		if (currentElement == XMLEntity.MULTIPLYING.value()){
			flower.setMultiplying(elementText);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (localName == XMLEntity.FLOWER.value()){
			flowers.getFlower().add(flower);
		}
		if (localName == XMLEntity.VISUAL_PARAMETERS.value()){
			flower.setVisualParameters(vp);
		}
		if (localName == XMLEntity.GROWING_TIPS.value()){
			flower.setGrowingTips(growingTips);
		}
	}

	public static void main(String[] args) throws ParserConfigurationException,
			SAXException, IOException {

		// try to parse valid XML file (success)
		SAXController saxContr = new SAXController(Constants.XML_FILE_NAME);
		saxContr.parse(true); // <-- do parse with validation on (success)
		Flowers test = saxContr.getFlowers(); // <-- obtain container

		// we have Test object at this point:
		System.out.println("====================================");
		System.out.print("Here is the test: \n" + test);
		System.out.println("====================================");

		// now try to parse NOT valid XML (failed)
		saxContr = new SAXController(Constants.INVALID_XML_FILE);
		try {
			saxContr.parse(true); // <-- do parse with validation on (failed)
		} catch (Exception ex) {
			System.err.println("====================================");
			System.err.println("Validation is failed:\n" + ex.getMessage());
			System.err
					.println("Try to print test object:" + saxContr.getFlowers());
			System.err.println("====================================");
		}

		// and now try to parse NOT valid XML with validation off (success)
		saxContr.parse(false); // <-- do parse with validation off (success)

		// we have Test object at this point:
		System.out.println("====================================");
		System.out.print("Here is the test: \n" + saxContr.getFlowers());
		System.out.println("====================================");
	}

	// PLACE YOUR CODE HERE

}