package com.epam.rd.java.basic.task8.util;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class XSDValidator {

    private String xsd;

    public XSDValidator(String xsd) {
        this.xsd = xsd;
    }

    public void validate(String xml) {
        try {
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            schemaFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
            schemaFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            Schema schema = schemaFactory.newSchema(new File(xsd));
            Validator validator = schema.newValidator();

            validator.setErrorHandler(new ErrorHandler() {
                @Override
                public void warning(SAXParseException exception) throws SAXException {
                    System.err.println("Warm: "+ exception.getMessage());
                }

                @Override
                public void error(SAXParseException exception) throws SAXException {
                    System.err.println("Not valid: "+exception.getMessage());
                    throw exception;
                }

                @Override
                public void fatalError(SAXParseException exception) throws SAXException {
                   //DO NOTHING
                }
            });

            validator.validate(new StreamSource(new File(xml)));

        } catch (SAXException | IOException e) {
            throw new RuntimeException(e);
        }

    }
}
