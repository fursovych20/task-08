package com.epam.rd.java.basic.task8.entity;

import javax.xml.bind.annotation.*;


@XmlRootElement(name = "flower")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "name",
        "soil",
        "origin",
        "visualParameters",
        "growingTips",
        "multiplying"
})
public class Flower {

    @XmlElement(name = "name", required = true)
    private String name;
    @XmlElement(name = "soil", required = true)
    private String soil;
    @XmlElement(name = "origin",required = true)
    private String origin;
    @XmlElement(name = "visualParameters",required = true)
    private VisualParameters visualParameters;
    @XmlElement(name = "growingTips",required = true)
    private GrowingTips growingTips;
    @XmlElement(name = "multiplying", required = true)
    private String multiplying;

    public Flower() {

    }

    public Flower(String name, String soil, String origin, VisualParameters visualParameters, GrowingTips growingTips, String multiplying) {
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.visualParameters = visualParameters;
        this.growingTips = growingTips;
        this.multiplying = multiplying;
    }

    public String getName() {
        return name;
    }
    public void setName(String value) {
        this.name = value;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String value) {
        this.soil = value;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String value) {
        this.origin = value;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters value) {
        this.visualParameters = value;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(GrowingTips value) {
        this.growingTips = value;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String value) {
        this.multiplying = value;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", visualParameters=" + visualParameters +
                ", growingTips=" + growingTips +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }
}
