package com.epam.rd.java.basic.task8.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class Lighting {

    @XmlAttribute(name = "lightRequiring", required = true)
    protected String lightRequiring;

    public Lighting() {
    }

    public Lighting(String lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    public String getLightRequiring() {
        return lightRequiring;
    }

    public boolean isCorrect() {
        return lightRequiring != null;
    }

    public void setLightRequiring(String value) {
        this.lightRequiring = value;
    }

    @Override
    public String toString() {
        return "Lighting{" +
                "lightRequiring='" + lightRequiring + '\'' +
                '}';
    }
}