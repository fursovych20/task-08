package com.epam.rd.java.basic.task8.entity;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "growingTips")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "tempreture",
        "lighting",
        "watering"
})
public class GrowingTips {

    @XmlElement(name = "tempreture",required = true)
    private Tempreture tempreture;
    @XmlElement(name = "lighting", required = true)
    private Lighting lighting;
    @XmlElement(name = "watering")
    private Watering watering;

    public GrowingTips() {
    }

    public GrowingTips(Tempreture tempreture, Lighting lighting, Watering watering) {
        this.tempreture = tempreture;
        this.lighting = lighting;
        this.watering = watering;
    }

    public Tempreture getTempreture() {
        return tempreture;
    }

    public void setTempreture(Tempreture value) {
        this.tempreture = value;
    }

    public Lighting getLighting() {
        return lighting;
    }

    public void setLighting(Lighting value) {
        this.lighting = value;
    }

    public Watering getWatering() {
        return watering;
    }

    public void setWatering(Watering value) {
        this.watering = value;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "tempreture=" + tempreture +
                ", lighting=" + lighting +
                ", watering=" + watering +
                '}';
    }
}