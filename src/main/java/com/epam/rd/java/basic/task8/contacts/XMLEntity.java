package com.epam.rd.java.basic.task8.contacts;

public enum XMLEntity {
// these are tags
    FLOWER("flower"),FLOWERS("flowers"),NAME("name"), SOIL("soil"),ORIGIN("origin"),VISUAL_PARAMETERS("visualParameters"),
    STEM_COLOUR("stemColour"), LEAF_COLOUR("leafColour"), AVE_LEN_FLOWER("aveLenFlower"), GROWING_TIPS("growingTips"),
    TEMPRETURE("tempreture"), LIGHTING("lighting"), WATERING("watering"), MULTIPLYING("multiplying"),
// these are attributes
    MEASURE("measure"), LIGHT_REQUIRING("lightRequiring");
    private String value;

    public String value() {
        return value;
    }

    XMLEntity(String value) {
        this.value = value.intern();
    }
}
