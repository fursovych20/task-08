<?xml version="1.0" encoding="UTF-8"?>
<!-- XSL transformation -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:n="http://www.nure.ua" version="1.0">

    <xsl:template match="/n:flowers">
        <html>
            <head>
                <title>flowers</title>
                <style type="text/css">
                    td {border: 1px solid black; padding: 5px;}
                    table {border: 2px solid black;}
                </style>
            </head>
            <body>
                <table>
                    <xsl:apply-templates select="n:flower" />
                </table>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="n:flower">
        <tr>
            <td>
                <xsl:value-of select="n:name" />
            </td>
        </tr>
        <tr>
            <td>
                <xsl:value-of select="n:soil" />
            </td>
        </tr>
        <tr>
            <td>
                <xsl:value-of select="n:origin" />
            </td>
        </tr>
        <xsl:apply-templates select="n:visualParameters" />
        <xsl:apply-templates select="n:growingTips"/>
        <tr>
            <td>
                <xsl:value-of select="n:multiplying"/>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="n:visualParameters">
        <tr>
            <td>
                <xsl:value-of select="n:stemColour" />
            </td>
            <td>
                <xsl:value-of select="n:leafColour"/>
            </td>
            <td>
                <xsl:attribute name="measure"/>
                <xsl:value-of select="n:aveLenFlower"/>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="n:growingTips">
        <tr>
            <td>
                <xsl:value-of select="n:tempreture"/>
            </td>
            <td>
                <xsl:value-of select="attribute::n:lightRequiring"/>
            </td>
            <td>
                <xsl:value-of select="n:watering"/>
            </td>
        </tr>
    </xsl:template>

</xsl:stylesheet>